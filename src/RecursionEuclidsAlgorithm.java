import java.util.Scanner;
public class RecursionEuclidsAlgorithm {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int firstNum, secondNum;
	// the user will input 2 numbers to find its GCD or Greatest Common Divisor
	// then it will pass it in the 2nd method
		System.out.println("Enter two numbers: ");
		firstNum = scan.nextInt();
		secondNum = scan.nextInt();
	// print the output
		System.out.print("Greatest Common Divisor: ");
		System.out.print(gcd(firstNum, secondNum));
	}
		
	public static int gcd(int firstNum, int secondNum) {
	// returns true if condition is satisfied  
		if (secondNum == 0) {
			return firstNum;
		}
	// modulo operator to finds out the first remaining number which is smaller than the divisor 
	// return GCD of firstNum and secondNum to the main method
		return gcd(secondNum, firstNum % secondNum);
	}
}
